import React from 'react';
import ReactDOM from 'react-dom';

import App from './js/App';

import './styles/styles.scss';

window.initMap = () => ReactDOM.render(<App />, document.getElementById('root'));

