import React, { Component } from 'react';
import Storage from 'localforage';

import MapInterface from './Map';
import HistoryInteface from './History';

export default class App extends Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.plot = this.plot.bind(this);
        this.centerMap = this.centerMap.bind(this);

        this.openHistory = this.openHistory.bind(this);
        this.closeHistory = this.closeHistory.bind(this);
 
        this.openPlot = this.openPlot.bind(this);
        this.closePlot = this.closePlot.bind(this);

        this.showPlot = this.showPlot.bind(this);
        this.deletePlot = this.deletePlot.bind(this);
        this.deleteAll = this.deleteAll.bind(this);

        this.state = {
            historyActive: false,
        };

        Storage.config({ name: 'twig-pilot-data' });
        Storage.getItem("plots").then(this.setDefaults.bind(this));
    }

    openHistory() {
        this.setState({ historyActive: true });
    }

    closeHistory() {
        this.setState({ historyActive: false });
    }

    openPlot() { 
        this.setState({ plotActive: true });
    }

    closePlot() {
        this.setState({ plotActive: false });
    }

    setDefaults(plots) {
        if (!plots) {
            this.setDefaultStorage();
        };

        return this.setDefaultState(plots || []);
    }

    setDefaultStorage() {
        Storage.setItem("plots", []);
    }

    setDefaultState(plots) {
        this.setState({
            features: {
                labels: true,
                terrain: false,
                earthquakes: false,
                volcanoes: false,
                plates: false,
                recent: false,
            },
            center: false,
            plots,
            historyActive: false
        });
    }

    plot(data) {
        const point = {
            mag: Number(data.mag),
            lat: data.north ? Number(data.lat) : -1 * Number(data.lat),
            lng: data.east ? Number(data.lng) : -1 * Number(data.lng),
            date: new Date(),
            place: data.place,
        };

        Storage.getItem("plots").then(plots => this.addPlot(point, plots));
    }

    addPlot(point, plots) {
        const nextPlots = [...plots, point];
        const center = { lat: point.lat, lng: point.lng };

        Storage.setItem("plots", nextPlots);

        const features = this.showRecentPlots(this.state.features);

        this.setState({
            plots: nextPlots,
            features,
            center,
        });
    }

    centerMap({ lat, lng, north, east }) {
        let targetLat = this.getTargetLat(lat, north);
        let targetLng = this.getTargetLng(lng, east);

        if (this.isDefined(targetLat) && this.isDefined(targetLng)) {
            this.setState({
                center: {
                    lat: targetLat,
                    lng: targetLng,
                }
            });
        }
    }

    getTargetLat(lat, north) {
        if (!this.isDefined(lat)) {
            return this.state.lat;
        }

        return north ? Number(lat) : -1 * Number(lat);
    }

    getTargetLng(lng, east) {
        if (!this.isDefined(lng)) {
            return this.state.lng;
        }

        return east ? Number(lng) : -1 * Number(lng);
    }

    isDefined(value) {
        return typeof value !== 'undefined' &&
               value !== '';
    }

    showRecentPlots(features) {
        return {
            ...features,
            recent: true,
        }
    }

    toggle(feature) {
        const features = { ...this.state.features };
        features[feature] = !this.state.features[feature];
        this.setState({ features, center: undefined });
    }

    showPlot({ lat, lng }) {
        this.setState({
            historyActive: false,
            features: { 
                ...this.state.features,
                recent: true,
            },
            center: {
                lat,
                lng
            }
        });
    }

    deletePlot(plot) {
        // TODO
    }

    deleteAll() {
        // TODO
    }

    render() {
        if (!this.state.features) {
            return <div></div>;
        }

        return <div>
            <MapInterface
              features={this.state.features}
              toggle={this.toggle}
              plot={this.plot}
              centerMap={this.centerMap}
              center={this.state.center}
              plots={this.state.plots}
              active={!this.state.historyActive}
              openPlot={this.openPlot}
              closePlot={this.closePlot}
              plotActive={this.state.plotActive}
            />
            <HistoryInteface
              plots={this.state.plots}
              open={this.openHistory}
              close={this.closeHistory}
              active={this.state.historyActive}
              showPlot={this.showPlot}
              deletePlot={this.deletePlot}
              deleteAll={this.deleteAll}
            />
        </div>;
    }
}
