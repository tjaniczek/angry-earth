import React, { Component } from 'react';

import Plot from './Plot';

export default class PlotInterface extends Component { 
    render() {
        if (!this.props.active) {
            return <div className="plot">
                <button className="button active action plot__open" onClick={this.props.open}>
                    Add an earthquake
                </button>
            </div>;
        }

        return <div className="plot active">
            <button className="button action plot__close" onClick={this.props.close}>
                Cancel
            </button>
            <Plot
              plot={this.props.plot}
              close={this.props.close}
              centerMap={this.props.centerMap}
              />
        </div>;
    }
}
