import React, { Component } from 'react';
import Storage from 'localforage';

export default class Plot extends Component {
    constructor(props) {
        super(props);

        this.handleChange = this.handleChange.bind(this);
        this.addPoint = this.addPoint.bind(this);
        this.toggleLat = this.toggleLat.bind(this);
        this.toggleLng = this.toggleLng.bind(this);

        this.state = this.getDefaultState();
    }

    getDefaultState() {
        return {
            place: '',
            mag: '',
            lat: '',
            north: true,
            lng: '',
            east: true,
        }
    }

    toggleLat() {
      const nextState = {
        ...this.state,
        north: !this.state.north,
      }

      this.props.centerMap(nextState);
      this.setState(nextState);
    }

    toggleLng() {
      const nextState = {
        ...this.state,
        east: !this.state.east,
      }

      this.props.centerMap(nextState);
      this.setState(nextState);
    }

    addPoint(event) {
        event.preventDefault();
        this.props.plot(this.state);
        this.setState(this.getDefaultState());
        this.props.close();
    }

    handleChange(event, category) {
        const nextState = { ...this.state };
        nextState[category] = event.target.value;

        if (category === 'lat' || category === 'lng') {
          this.props.centerMap(nextState);
        }

        this.setState(nextState);
    }

    render() {
        return  <div className="plot__interface" >
            <h3>Add an earthquake</h3>

            <form onSubmit={this.addPoint}>
                <div className="plot__direction">
                    <input
                      type="number"
                      required
                      step="any"
                      min="0" max="90"
                      placeholder="Latitude"
                      value={this.state.lat}
                      onChange={event => this.handleChange(event, 'lat')}
                    />
                    <input
                      type="button"
                      className="button active action plot__direction"
                      value={this.state.north ? 'North' : 'South' }
                      onClick={this.toggleLat}
                    />
                </div>

                <div className="plot__direction">
                    <input
                      type="number"
                      required
                      step="any"
                      min="0" max="180"
                      placeholder="Longitude"
                      value={this.state.lng}
                      onChange={event => this.handleChange(event, 'lng')}
                    />
                    <input
                      type="button"
                      className="button active action plot__direction"
                      value={this.state.east ? 'East' : 'West'}
                      onClick={this.toggleLng}
                    />
                </div>

                <input
                  type="number"
                  required
                  step="any"
                  min="1" max="12"
                  placeholder="Magnitude (in Richter scale)"
                  value={this.state.mag}
                  onChange={event => this.handleChange(event, 'mag')}
                />

                <input
                  type="text"
                  required
                  placeholder="Place"
                  value={this.state.place}
                  onChange={event => this.handleChange(event, 'place')}
                />

                <input type="submit" className="button active action float-right" value="Save"></input>
            </form>
        </div>;
    }
}
