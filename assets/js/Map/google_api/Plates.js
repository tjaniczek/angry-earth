import settings from './settings/plates';

export default class Plates {
    constructor(map) {
        this.map = map;
        this.plates = new google.maps.KmlLayer(settings);
    }

    toggle(show) {
        if (show === this.visible) {
            return undefined;
        }

        this.visible = show;

        const target = show ? this.map : null;
        this.plates.setMap(target);
    }
}
