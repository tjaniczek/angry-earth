export default [
    {elementType: "geometry", stylers: [{ color: '#2d5d2d' }]},
    {elementType: "geometry", featureType: "water", stylers: [{color: "#171a46",}]},
    {elementType: "geometry", featureType: "administrative.country",stylers: [{color: "#28441a",},],},
    {elementType: "labels", featureType: "administrative.country",stylers: [{visibility: "on",},],},
    {elementType: "labels", stylers: [{ visibility: "off" }]},
    {elementType: 'labels.text.stroke', stylers: [{color: '#2c3a2c'}]},
    {elementType: 'labels.text.fill', stylers: [{color: '#dee0d8'}]},
];
