export default {
    center: {
        lat: 40,
        lng: -100
    },
    mapTypeControl: false,
    streetViewControl: false,
    fullscreenControl: false,
    zoom: 4,
    minZoom: 3,
    maxZoom: 7,
    backgroundColor: '#171a46'
}
