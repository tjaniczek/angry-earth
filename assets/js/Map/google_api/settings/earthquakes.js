export default {
    strokeColor: '#000',
    strokeOpacity: 1,
    strokeWeight: 1,
    fillColor: '#45aee2',
    fillOpacity: 0.9,
    map: null,
    scale: 12,
};
