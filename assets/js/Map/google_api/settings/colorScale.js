// No point to make it dynamic at this point
// https://gka.github.io/palettes

export default [
    '#42a5cf',
    '#5daac5',
    '#71afbc',
    '#80b5b3',
    '#8db9a9',
    '#9abf9f',
    '#a4c495',
    '#afcb8a',
    '#b7d080',
    '#c0d674',
    '#c9dc68',
    '#d1e15a',
    '#d9e74a',
    '#e0ed38',
    '#e7f31b',
    '#eaea18',
    '#ede316',
    '#f0da13',
    '#f2d211',
    '#f4c90e',
    '#f6c20c',
    '#f8b809',
    '#fab007',
    '#fba605',
    '#fc9e03',
    '#fd9502',
    '#fe8b01',
    '#fe8201',
    '#ff7700',
    '#ff6c00',
    '#ff6c00'
];
