export default {
    strokeColor: '#000',
    strokeWeight: 1,
    fillColor: '#bd5217',
    fillOpacity: 1,
    scale: 6,
};
