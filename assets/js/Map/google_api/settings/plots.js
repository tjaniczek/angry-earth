export default {
    strokeColor: '#000000',
    strokeOpacity: 1,
    strokeWeight: 2,
    fillColor: '#fbc021',
    fillOpacity: 1,
    map: null,
    scale: 12,
};
