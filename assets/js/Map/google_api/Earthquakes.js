import data from './data/earthquakes';
import settings from './settings/earthquakes';
import colors from './settings/colorScale';
import moment from 'moment';

export default class Earthquakes {
    constructor(map) {
        this.map = map;
        this.points = data.map(this.getPoint.bind(this));
    }

    getPoint(details) {
        const icon = this.createIcon(details);
        const popup = this.createPopup(details);

        const point = new google.maps.Marker(icon);
        point.popup = popup;

        point.setVisible(false);
        point.setMap(this.map);

        return point;
    }

    createIcon({ lat, lng, mag }) {
        const position = { lat, lng };
        const fillColor = this.getColorCode(mag);

        const icon = {
            ...settings,
            path: google.maps.SymbolPath.CIRCLE,
            fillColor,
        };

        return { icon, position };
    }

    getColorCode(mag) {
        // Converts magnitude to scale from 0 to 30
        const base = Math.round(mag * 3);
        const index = Math.max(0, Math.min(30, base));
        return colors[index];
    }

    createPopup({ country = '', region, date, mag }) {
        const day = moment(date).format('LL');
        const content = `<h3>${region}<small>, ${country}</small></h3>
                      <p><strong>Date</strong>: ${day}</p>
                      <p><strong>Magnitude</strong>: ${mag}`;

        return new google.maps.InfoWindow({ content });
    }

    toggle(show) {
        this.points.forEach(point => point.setVisible(show));
    }
}
