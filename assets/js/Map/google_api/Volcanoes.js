import data from './data/volcanoes';
import settings from './settings/volcanoes';

export default class Volcanoes {
    constructor(map) {
        this.map = map;
        this.points = data.map(this.getPoint.bind(this));

         console.warn('Displaying subset of volcanos');
         // this.points = data.splice(0,150).map(this.getPoint.bind(this));
    }

    getPoint(details) {
        const icon = this.createIcon(details);
        const popup = this.createPopup(details);

        const point = new google.maps.Marker(icon);
        point.popup = popup; 

        point.setVisible(false);
        point.setMap(this.map);

        return point;
    }

    createIcon({ lat, lng }) {
        const position = { lat, lng };
        const icon = {
            ...settings,
            path: google.maps.SymbolPath.FORWARD_OPEN_ARROW,
        };

        return { icon, position };
    }

    createPopup({ name, country, type, elevation }) {
        const content = `<h3>${name} <small>${country}</small></h3>
                         <p><strong>Elevation</strong>: ${elevation}m</p>
                         <p><strong>Type</strong>: ${type}</p>`;
        return new google.maps.InfoWindow({ content });
    }

    toggle(show) {
        this.points.forEach(point => point.setVisible(show));
    }

}
