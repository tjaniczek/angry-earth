import settings from './settings/plots';
import colors from './settings/colorScale';
import moment from 'moment';

export default class Plots {
    constructor(map, plots) {
        this.map = map;
        this.points = [];
        this.updateData(plots);
    }

    updateData(plots = []) {
        this.points.forEach(this.removePoint);
        this.points = plots.map(point => this.getPoint(point));
    }

    removePoint(point) {
        point.popup.close();
        point.popup = null;
        point.reference = null;
        point.setMap(null);
    }

    getPoint(details) {
        const icon = this.createIcon(details);
        const popup = this.createPopup(details);

        const point = new google.maps.Marker(icon);
        point.popup = popup;

        point.reference = details;

        point.setVisible(false);
        point.setMap(this.map);

        return point;
    }

    createIcon({ lat, lng, mag }) {
        const position = { lat, lng };
        const fillColor = this.getColorCode(mag);

        const icon = {
            ...settings,
            path: google.maps.SymbolPath.CIRCLE,
            fillColor,
        };

        return { icon, position };
    }

    getColorCode(mag) {
        // Converts magnitude to scale from 0 to 30
        const base = Math.round(mag * 3);
        const index = Math.max(0, Math.min(30, base));
        return colors[index];
    }

    createPopup({ place = '', date, mag }) {
        const day = moment(date).format('LL');
        const time = moment(date).format('LT');
        const content = `<h3>${place}</h3>
                      <p><strong>Date</strong>: ${day}</p>
                      <p><strong>Time</strong>: ${time}</p>
                      <p><strong>Magnitude</strong>: ${mag}`;

        return new google.maps.InfoWindow({ content });
    }

    toggle(show) {
        this.points.forEach(point => point.setVisible(show));
    }
}
