import React, { Component } from 'react';
import classNames from 'classnames';

export default class Controls extends Component {
    render() {
        const {
            toggle,
            labels,
            terrain,
            earthquakes,
            volcanoes,
            plates,
            recent,
        } = this.props; 

        const showScale = earthquakes || recent;

        return <div>
            <div className="navigation">
                <button
                  type="button"
                  className={classNames({ button: true, active: labels })}
                  onClick={() => toggle('labels')}
                >
                    Countries
                </button>
                <button
                  type="button"
                  className={classNames({ button: true, active: terrain })}
                  onClick={() => toggle('terrain')}
                >
                    Terrain
                </button>
                <button
                  type="button"
                  className={classNames({ button: true, active: plates })}
                  onClick={() => toggle('plates')}
                >
                    Tectonic plates
                </button>
                <button
                  type="button"
                  className={classNames({ button: true, active: volcanoes })}
                  onClick={() => toggle('volcanoes')}
                >
                    Volcanoes
                </button>
                <button
                  type="button"
                  className={classNames({ button: true, active: earthquakes })}
                  onClick={() => toggle('earthquakes')}
                >
                    Historic earthquakes
                </button>
                <button
                  type="button"
                  className={classNames({ button: true, active: recent })}
                  onClick={() => toggle('recent')}
                >
                    Recent earthquakes
                </button>
            </div>
            <div className={classNames({ 'color-scale': true, active: showScale })}>
                <h4>Magnitude (Richter scale)</h4>
                <table>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>2</td>
                            <td>3</td>
                            <td>4</td>
                            <td>5</td>
                            <td>6</td>
                            <td>7</td>
                            <td>8</td>
                            <td>9</td>
                            <td>10</td>
                        </tr>
                    </tbody>
                </table>
                <div className="color-scale__bar"></div>
            </div>
        </div>;
    }
}
