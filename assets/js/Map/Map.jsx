import React, { Component } from 'react';

import settings from './google_api/settings/map';
import styles from './google_api/settings/styles';

import Plates from './google_api/Plates';
import Earthquakes from './google_api/Earthquakes';
import Volcanoes from './google_api/Volcanoes';
import Plots from './google_api/Plots';

export default class Map extends Component {
    componentDidMount() {
        this.map = new google.maps.Map(this.refs.map, { ...settings, styles });

        this.createDataLayers(this.map, this.props.plots);
        this.createAllPopups();
        this.toggleLayers(this.props);
    }

    componentWillReceiveProps(nextProps) {
        this.addNewUserPlots(nextProps);
        this.toggleLayers(nextProps);
        this.centerMap(nextProps);
    }

    addNewUserPlots({ plots }) {
        if (this.props.plots.length !== plots.length) {
            this.userPlots.updateData(plots);
            this.createLayerPopups(this.userPlots);
        }
    }

    centerMap({ center }) { 
        console.log(center)
        if (center) {
            this.map.panTo(center);
            const point = this.findUserPoint(center);

            if (point && point.popup) {
                this.openPopup(point);
            }
        }
    } 

    findUserPoint({ lat, lng }) {
        return this.userPlots.points.find(point => (
            (point.reference.lat === lat) && (point.reference.lng === lng)
        ));
    }

    createDataLayers(map, plots) {
        this.userPlots = new Plots(map, plots);
        this.plates = new Plates(map);
        this.earthquakes = new Earthquakes(map);
        this.volcanoes = new Volcanoes(map);
    }

    createAllPopups() {
        this.createLayerPopups(this.volcanoes);
        this.createLayerPopups(this.earthquakes);
        this.createLayerPopups(this.userPlots);
    }

    closeAllPopups() {
        this.closeLayerPopups(this.volcanoes);
        this.closeLayerPopups(this.earthquakes);
        this.closeLayerPopups(this.userPlots);
    }

    createLayerPopups(layer) {
        layer.points.map(this.addPopupEvent.bind(this));
    }

    closeLayerPopups(layer) {
        layer.points.forEach(point => point.popup.close());
    }

    addPopupEvent(point) {
        point.addListener('click', this.openPopup.bind(this, point));
    }

    openPopup(point) {
        this.closeAllPopups();
        point.popup.open(this.map, point);
    }

    toggleLayers({ earthquakes, volcanoes, plates, recent, labels, terrain, active }) {
        this.plates.toggle(plates);
        this.earthquakes.toggle(earthquakes);
        this.volcanoes.toggle(volcanoes);
        this.userPlots.toggle(recent);
        this.toggleMapStyles(labels, terrain);

        if (!volcanoes) {
            this.closeLayerPopups(this.volcanoes);
        }

        if (!earthquakes) {
            this.closeLayerPopups(this.earthquakes);
        }

        if (!recent) {
            this.closeLayerPopups(this.userPlots);
        }

        if (!active) {
            this.closeAllPopups();
        }
    }

    toggleMapStyles(labels, terrain) {
        const labelsStyles = [
            {
                featureType: "administrative.country",
                elementType: "geometry",
                stylers: [{
                    visibility: labels ? 'on' : 'off'
                }]
            },
            {
                featureType: "administrative.country",
                elementType: "labels",
                stylers: [{
                    visibility: labels ? 'on' : 'off'
                }]
            }
        ];

        const terrainStyles = [
              {
                "featureType": "landscape.natural.terrain",
                "elementType": "geometry",
                "stylers": [
                    {
                        color: '#549f54'
                    },
                    {
                        visibility: terrain ? 'on' : 'off'
                    },
                ]
            }
        ];

        this.map.setOptions({
            styles: styles.concat(labelsStyles, terrainStyles)
        });
    }


    render() {
        return <div id="map" ref="map"></div>;
    }
}
