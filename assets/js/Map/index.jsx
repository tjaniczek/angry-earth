import React, { Component } from 'react'; 
import classNames from 'classnames';

import Controls from './Controls';
import Map from './Map';
import PlotInterface from '../Plot'; 

const MapInterface = ({ 
  features, toggle, plot, centerMap, center, plots, active, openPlot, closePlot, plotActive 
}) => (
    <div className={classNames({ 'map-interface': true, active })}>
        <Controls
          {...features}
          toggle={toggle}
        />
        <PlotInterface
          plot={plot}
          centerMap={centerMap}
          open={openPlot}
          close={closePlot}
          active={plotActive}
        />
        <Map
          {...features}
          center={center}
          plots={plots}
          active={!plotActive}
        />
        <div className="dev">Status: <strong>Pre-Alpha</strong> v0.0.23 <small>(most features are ready)</small><br/>Last deployment: <strong>Monday, 1st</strong></div>
    </div>
);

export default MapInterface;