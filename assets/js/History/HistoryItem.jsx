import React, { Component } from 'react';
import Storage from 'localforage';
import uuid from 'uuid';
import moment from 'moment';

import HistoryItem from './HistoryItem';

export default class HistoryInterface extends Component {
    componentWillMount() {
        const time = moment(this.props.plot.date).format('lll');
        const mag = this.props.mag;

        let lng = Number(this.props.plot.lng).toFixed(2);
        let lngLabel = 'E';
        if (lng < 0) {
          lng = lng * -1;
          lngLabel = 'W';
        }


        let lat = Number(this.props.plot.lat).toFixed(2);
        let latLabel = 'N';
        if (lat < 0) {
          lat = lat * -1;
          latLabel = 'S';
        }

        this.setState({
          lat,
          latLabel,
          lng,
          lngLabel,
          time,
          mag: this.props.plot.mag,
          place: this.props.plot.place || '-',
        });
    }

    render() {
        return  <tr>
            <td><strong>{this.state.place}</strong></td>
            <td className="text-right">
                {this.state.lat}&deg; {this.state.latLabel}
            </td>
            <td className="text-right">
                {this.state.lng}&deg; {this.state.lngLabel}
            </td>
            <td className="text-center">
              <strong>{this.state.mag}</strong>
            </td>
            <td>{this.state.time}</td>
            <td className="text-center">
                <input
                  type="button"
                  className="button active"
                  value="Show"
                  onClick={() => this.props.show(this.props.plot)}
                />
            </td>
            <td className="text-center">
                <input
                  type="button"
                  className="button active action"
                  value="Delete"
                  onClick={() => this.props.deletePlot(this.props.plot)}
                />
            </td>
        </tr>;
    }
}
