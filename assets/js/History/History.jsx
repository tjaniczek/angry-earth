import React, { Component } from 'react';
import Storage from 'localforage';
import uuid from 'uuid';
import moment from 'moment';

import HistoryItem from './HistoryItem';

export default class History extends Component {
    constructor(props) {
        super(props);

        this.deletePlot = this.deletePlot.bind(this);
        this.deleteAll = this.deleteAll.bind(this);
        this.cancelDelete = this.cancelDelete.bind(this);

        this.confirmSingle = this.confirmSingle.bind(this);
        this.confirmAll = this.confirmAll.bind(this);

        this.state = { 
            deletePlot: false,
            deleteAll: false, 
            list: props.plots.reverse(),
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            list: nextProps.plots.reverse()
        });
    }

    deletePlot(plot) {
        window.scrollTo(0,0);
        console.log('heheh')
        this.setState({
            deletePlot: plot,
            deleteAll: false,
        })
    }

    deleteAll(plot) {
        this.setState({
            deletePlot: false,
            deleteAll: true,
        });
    }

    cancelDelete(plot) {
        this.setState({
            deletePlot: false,
            deleteAll: false,
        });
    }
    
    confirmSingle() {
        this.props.deletePlot(this.state.deletePlot);

        this.setState({
            deletePlot: false,
            deleteAll: false,
        });
    }
    
    confirmAll() {
        this.props.deleteAll();

        this.setState({
            deletePlot: false,
            deleteAll: false,
        });
    }

    render() { 
        const show = this.props.showPlot;  

        return  <div className="history__interface">
            <div className="row">
                <div className="small-12 columns">
                <br /><br />
                    <h2>Recent earthquakes</h2>
                </div>
            </div>
            <div className="row">
                <div className="small-9 columns">
                    { this.state.list.length > 0 &&
                        <table className="history__table">
                        <thead>
                            <tr>
                                <td>Place</td>
                                <td>Latitude</td>
                                <td>Longitude</td>
                                <td>Magnitude</td>
                                <td>Time recorded</td>
                                <td colSpan="2">Actions</td>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.list.map(
                                plot => <HistoryItem 
                                          key={uuid()} 
                                          plot={plot} 
                                          show={show} 
                                          deletePlot={this.deletePlot}
                                        />
                            )}
                        </tbody>
                    </table>
                    }
                    { this.state.list.length <= 0 && 
                        <div>
                            You have no saved earthquakes yet.
                        </div>
                    }
                </div>
                <div className="small-3 columns history__sidebar">

                    { this.state.deletePlot &&
                        <div>
                            <div className="callout secondary">
                                <h3>Delete point</h3>
                                <p>
                                Are you sure you want to delete earthquake data in 
                                <strong> {this.state.deletePlot.place}</strong>?
                                </p>
                                <div className="clearfix">
                                    <input
                                      type="button"
                                      className="button secondary float-right"
                                      value="Cancel"
                                      onClick={this.cancelDelete}
                                    />
                                    <input
                                      type="button"
                                      className="button active action float-right"
                                      value="Delete"
                                      onClick={this.confirmSingle}
                                    />
                                </div>
                            </div>
                        </div>
                    }
                    { this.state.deleteAll &&
                        <div>
                            <div className="callout secondary">
                                <h3>Erase all data</h3>
                                <p>
                                Are you sure you want to erase all previously saved data?
                                You won't be able to take back this operation.
                                </p>
                                <div className="clearfix">
                                    <input
                                      type="button"
                                      className="button secondary float-right"
                                      value="Cancel"
                                      onClick={this.cancelDelete}
                                    />
                                    <input
                                      type="button"
                                      className="button active action float-right"
                                      value="Erase all"
                                      onClick={this.confirmAll}
                                    />
                                </div>
                            </div>
                        </div>
                    }
                    { !this.state.deletePlot && !this.state.deleteAll &&
                        <div>
                            
                            Here's place for additional info and <strong>delete all</strong> button.
                            <br/><br/ >
                            We could add a word or two about Twig World and the pilot program too.
                            <br/>
                            <input
                              type="button"
                              className="button active action"
                              value="Erase all data"
                              onClick={() => this.deleteAll()}
                            />
                        </div>
                    }
                </div>
            </div>
        </div>;
    }
}
