import React, { Component } from 'react';

import History from './History';

const HistoryInterface = (props) => { 
    if (!props.active) {
        return <div className="history"> 
            <button className="button active action history__open" onClick={props.open}>
                <div className="icon"></div>
            </button>
        </div>;
    }

    return <div className="history active">
        <button className="button active action history__close" onClick={props.close}>
            <div className="icon"></div>
        </button>
        <History
          plots={props.plots}
          showPlot={props.showPlot}
          deletePlot={props.deletePlot}
          deleteAll={props.deleteAll}
        />
    </div>; 
}

export default HistoryInterface;