var path = require('path');
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var session = require('express-session');

app.set('port', (process.env.PORT || 5000));

app.use(express.static('static'));
app.use(express.static('dist'));
app.use(session({
  secret: 'AkademiaNauk',
    resave: true,
    saveUninitialized: false,
    cookie: {
        secure: false,
        maxAge: 864000000
    }
}));

app.use(bodyParser.text());

app.use(function(req, res, next) {
    if (req.url === '/' && (!req.session || !req.session.authenticated)) {

        res.redirect('/login');
        return;
    }

    next();
})

app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname + '/index.html'));
});

app.route('/login')
  .get(function (req, res) {
    res.sendFile(path.join(__dirname + '/login.html'));
  })
  .post(function (req, res) {
    if (req.body === 'password=tw1g') {
        req.session.authenticated = true;
        req.session.save(function() {
            res.redirect('/');
        });
    }
  })

app.listen(app.get('port'), function () {
  console.log('Example app listening on port 5000!');
})
