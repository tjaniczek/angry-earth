var path = require('path');

module.exports = {
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                loader: "babel-loader"
            },
            {
                test: /\.scss$/,
                use: [
                    { loader: "style-loader" },
                    { loader: "css-loader" },
                    { loader: "sass-loader" },
                ],
            }
        ],
    },

    entry: './assets/index.js',

    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'dist')
    },

    resolve: {
      extensions: ['.js', '.jsx']
    },

    devServer: {
        contentBase: [
            __dirname,
            path.join(__dirname, "static")
        ]
    }
};
